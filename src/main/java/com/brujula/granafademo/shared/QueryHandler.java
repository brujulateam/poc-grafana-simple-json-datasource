package com.brujula.granafademo.shared;

public interface QueryHandler<R, Q> {
    R handle(final Q query);
}
