package com.brujula.granafademo.application.query.errors;

import com.brujula.granafademo.domain.errors.model.ErrorBloqueo;
import com.brujula.granafademo.domain.errors.repository.ErrorRepository;
import com.brujula.granafademo.shared.QueryHandler;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public final class ObtainErrorsQueryHandler implements QueryHandler<List<ErrorBloqueo>, ObtainErrorsQuery> {

    private final ErrorRepository errors;

    public ObtainErrorsQueryHandler(ErrorRepository errors) {
        this.errors = errors;
    }

    @Override
    public List<ErrorBloqueo> handle(ObtainErrorsQuery query) {
        return errors.byIdAgency(query.getAgency(), query.getLimit() , query.getFrom(), query.getTo());
    }
}

