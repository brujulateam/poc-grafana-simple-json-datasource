package com.brujula.granafademo.application.query.errors;

import java.time.LocalDateTime;

public  class ObtainErrorsQuery {

    private final String agency;
    private final LocalDateTime from;
    private final LocalDateTime to;
    private final Integer limit;

    public ObtainErrorsQuery(final String agency, final Integer limit, final LocalDateTime from, final LocalDateTime to) {
        this.agency = agency;
        this.from = from;
        this.to = to;
        this.limit = limit;
    }

    public String getAgency() {
        return agency;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public Integer getLimit() {
        return limit;
    }
}
