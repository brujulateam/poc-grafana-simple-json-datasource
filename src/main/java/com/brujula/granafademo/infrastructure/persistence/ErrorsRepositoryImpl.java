package com.brujula.granafademo.infrastructure.persistence;

import com.brujula.granafademo.domain.errors.model.ErrorBloqueo;
import com.brujula.granafademo.domain.errors.repository.ErrorRepository;
import com.brujula.granafademo.infrastructure.persistence.entity.BloqErrorEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ErrorsRepositoryImpl implements ErrorRepository {

    private IErrorsRepositoryJPA jpaRepository;

    public ErrorsRepositoryImpl(IErrorsRepositoryJPA jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    @Override
    public List<ErrorBloqueo> byIdAgency(final String agency, Integer limit, final LocalDateTime from, final LocalDateTime to) {
        List<BloqErrorEntity> persistenceEntity = jpaRepository.byIdAgency(agency, limit, from, to);
        return persistenceEntity == null ? new ArrayList<>() : persistenceEntity.stream().map(
                e -> new ErrorBloqueo(e.getBeginning(), e.getErrors())).collect(Collectors.toList());
    }
}
