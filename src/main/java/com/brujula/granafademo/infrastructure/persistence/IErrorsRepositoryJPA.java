package com.brujula.granafademo.infrastructure.persistence;

import com.brujula.granafademo.infrastructure.persistence.entity.BloqErrorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;


public interface IErrorsRepositoryJPA extends JpaRepository<BloqErrorEntity, Double> {

    @Query(value = "SELECT LBW_FECHA_INICIO, COUNT(1) as ERRORS \n" +
            "FROM LOG_ERRORES_BLQ_WIS errors\n" +
            "WHERE LBW_ERROR IS NOT NULL\n" +
            "AND LBW_AGENCIA LIKE %:agency%\n" +
            "AND LBW_FECHA_INICIO >= :from\n" +
            "AND LBW_FECHA_INICIO <= :to\n" +
            "GROUP BY LBW_AGENCIA, LBW_FECHA_INICIO\n" +
            "ORDER BY LBW_FECHA_INICIO ASC\n" +
            "LIMIT :limit", nativeQuery = true)
    List<BloqErrorEntity> byIdAgency(final String agency, final Integer limit, final LocalDateTime from, final LocalDateTime to);
}
