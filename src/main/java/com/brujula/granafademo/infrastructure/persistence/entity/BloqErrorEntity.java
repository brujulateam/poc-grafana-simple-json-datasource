package com.brujula.granafademo.infrastructure.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class BloqErrorEntity {

    @Id
    @Column(name = "LBW_FECHA_INICIO", columnDefinition = "TIMESTAMP")
    private LocalDateTime beginning;

    @Column(name = "ERRORS")
    private Integer errors;


    public LocalDateTime getBeginning() {
        return beginning;
    }

    public void setBeginning(LocalDateTime beginning) {
        this.beginning = beginning;
    }

    public Integer getErrors() {
        return errors;
    }

    public void setErrors(Integer errors) {
        this.errors = errors;
    }
}
