package com.brujula.granafademo.infrastructure.api.dto;

public class TagValueQuery {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
