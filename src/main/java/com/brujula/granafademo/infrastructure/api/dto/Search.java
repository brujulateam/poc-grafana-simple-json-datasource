package com.brujula.granafademo.infrastructure.api.dto;

import java.io.Serializable;

public class Search implements Serializable {
    private String target;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
