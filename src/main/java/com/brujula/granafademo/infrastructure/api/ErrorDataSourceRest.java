package com.brujula.granafademo.infrastructure.api;


import com.brujula.granafademo.application.query.errors.ObtainErrorsQuery;
import com.brujula.granafademo.domain.errors.model.ErrorBloqueo;
import com.brujula.granafademo.infrastructure.api.dto.Annotation;
import com.brujula.granafademo.infrastructure.api.dto.Annotations;
import com.brujula.granafademo.infrastructure.api.dto.AnnotationsQuery;
import com.brujula.granafademo.infrastructure.api.dto.DataPoints;
import com.brujula.granafademo.infrastructure.api.dto.Query;
import com.brujula.granafademo.infrastructure.api.dto.Range;
import com.brujula.granafademo.infrastructure.api.dto.Search;
import com.brujula.granafademo.infrastructure.api.dto.TagKey;
import com.brujula.granafademo.infrastructure.api.dto.TagValue;
import com.brujula.granafademo.infrastructure.api.dto.TagValueQuery;
import com.brujula.granafademo.shared.QueryHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "error-datasource")
public class ErrorDataSourceRest {

    private Logger logger = LoggerFactory.getLogger(ErrorDataSourceRest.class);

    private final QueryHandler<List<ErrorBloqueo>, ObtainErrorsQuery> queryHandler;

    public ErrorDataSourceRest(QueryHandler<List<ErrorBloqueo>, ObtainErrorsQuery> queryHandler) {
        this.queryHandler = queryHandler;
    }

    @GetMapping
    public void health() {
        //doNothing
    }

    @PostMapping("/search")
    public List<String> search(final @RequestBody Search search) {
        //doNothing
        return Arrays.asList("errors_A", "errors_B");
    }

    @PostMapping("/query")
    public List<DataPoints> query(final @RequestBody @Valid Query<Map<String,Object>> query) {
        List<DataPoints> dataPoints = new ArrayList<>();

        query.getTargets().stream().forEach(
                target -> {
                    Range range = query.getRange();
                    List<ErrorBloqueo> queryResult = queryHandler.handle(new ObtainErrorsQuery("B2B", query.getMaxDataPoints(), range.getFrom(), range.getTo()));
                    DataPoints targetDatapoints = new DataPoints();
                    targetDatapoints.setTarget(target.getTarget());
                    List<List<Object>> datapointRetrieved = new ArrayList<>();
                    if ( queryResult != null ) {
                        queryResult.stream().forEach( error -> {
                            datapointRetrieved.add(
                                    Arrays.asList(error.getErrors(),
                                            error.getTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()));
                        });
                    }
                    targetDatapoints.setDatapoints(datapointRetrieved);
                    dataPoints.add(targetDatapoints);
                }
        );
        return dataPoints;
    }

    @PostMapping("/annotations")
    public Annotations annotations(final @RequestBody AnnotationsQuery annotations){
        logger.info("annotations request");
        Annotations result = new Annotations();
        Annotation annotation = new Annotation();
        annotation.setTime(LocalDateTime.now().minusHours(4).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        annotation.setText("Test annotation");
        annotation.setTitle("Dummy");
        annotation.setTags("dummy");
        result.setAnnotations(Arrays.asList(annotation));
        return result;
    }

    @PostMapping("/tag-keys")
    public List<TagKey> tagKeys() {
        logger.info("tagKeys request");
        return Arrays.asList(new TagKey("text", "agency"));

    }

    @PostMapping("/tag-values")
    public List<TagValue> tagValues(@RequestBody TagValueQuery tagKey) {
        logger.info("tagKeys request"); //Prodria ser un select distinc providers
        return Arrays.asList(new TagValue("B2B"), new TagValue("ARZAB"));
    }
}
