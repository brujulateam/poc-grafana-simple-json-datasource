package com.brujula.granafademo.infrastructure.api.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Range implements Serializable {
    @DateTimeFormat
    private LocalDateTime from;
    private LocalDateTime to;
    private RangeRaw raw;

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }

    public RangeRaw getRaw() {
        return raw;
    }

    public void setRaw(RangeRaw raw) {
        this.raw = raw;
    }
}
