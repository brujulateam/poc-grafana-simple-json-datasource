package com.brujula.granafademo.infrastructure.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ScopeVars implements Serializable {

    @JsonProperty("__interval")
    private ScopeVar<String> interval;

    @JsonProperty("__interval_ms")
    private ScopeVar<Long> intervalMs;

    public ScopeVar<String> getInterval() {
        return interval;
    }

    public void setInterval(ScopeVar<String> interval) {
        this.interval = interval;
    }

    public ScopeVar<Long> getIntervalMs() {
        return intervalMs;
    }

    public void setIntervalMs(ScopeVar<Long> intervalMs) {
        this.intervalMs = intervalMs;
    }
}
