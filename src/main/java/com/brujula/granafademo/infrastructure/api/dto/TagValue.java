package com.brujula.granafademo.infrastructure.api.dto;

public class TagValue {
    private String text;

    public TagValue(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
