package com.brujula.granafademo.infrastructure.api.dto;

public class TagKey {
    private String type;
    private String text;

    public TagKey(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }


    public String getText() {
        return text;
    }

}
