package com.brujula.granafademo.infrastructure.api.dto;

public class Annotation {
    private AnnotationQuery annotation;
    private Long time;
    private String title;
    private String text;
    private String tags;

    public AnnotationQuery getAnnotation() {
        return annotation;
    }

    public void setAnnotation(AnnotationQuery annotation) {
        this.annotation = annotation;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
