package com.brujula.granafademo.infrastructure.api.dto;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.List;

public class Annotations {

    @JsonValue
    private List<Annotation> annotations;

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }
}
