package com.brujula.granafademo.infrastructure.api.dto;

import java.io.Serializable;

public class ScopeVar<T> implements Serializable {

    private String text;
    private T value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
