package com.brujula.granafademo.infrastructure.api.dto;

public class AnnotationsQuery {
    private Range range;
    private RangeRaw raw;
    private AnnotationQuery annotation;

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public RangeRaw getRaw() {
        return raw;
    }

    public void setRaw(RangeRaw raw) {
        this.raw = raw;
    }

    public AnnotationQuery getAnnotation() {
        return annotation;
    }

    public void setAnnotation(AnnotationQuery annotation) {
        this.annotation = annotation;
    }
}
