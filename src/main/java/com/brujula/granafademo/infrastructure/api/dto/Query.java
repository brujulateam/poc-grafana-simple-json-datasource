package com.brujula.granafademo.infrastructure.api.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class Query<D> implements Serializable {

    private String app;

    private String requestId;

    private String timezone;

    private Integer panelId;

    private Integer dashboardId;

    private Range range;

    private String interval;

    private Long intervalMs;

    @NotNull
    @Size(min = 1)
    private List<Target<D>> targets;

    private String format;

    private Integer maxDataPoints;

    private ScopeVars scopedVars;

    private Long startTime;

    private List<AdhocFilter> adhocFilters;

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public List<Target<D>> getTargets() {
        return targets;
    }

    public void setTargets(List<Target<D>> targets) {
        this.targets = targets;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Integer getMaxDataPoints() {
        return maxDataPoints;
    }

    public void setMaxDataPoints(Integer maxDataPoints) {
        this.maxDataPoints = maxDataPoints;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Integer getPanelId() {
        return panelId;
    }

    public void setPanelId(Integer panelId) {
        this.panelId = panelId;
    }

    public Integer getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(Integer dashboardId) {
        this.dashboardId = dashboardId;
    }

    public Long getIntervalMs() {
        return intervalMs;
    }

    public void setIntervalMs(Long intervalMs) {
        this.intervalMs = intervalMs;
    }

    public ScopeVars getScopedVars() {
        return scopedVars;
    }

    public void setScopedVars(ScopeVars scopedVars) {
        this.scopedVars = scopedVars;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public List<AdhocFilter> getAdhocFilters() {
        return adhocFilters;
    }

    public void setAdhocFilters(List<AdhocFilter> adhocFilters) {
        this.adhocFilters = adhocFilters;
    }
}
