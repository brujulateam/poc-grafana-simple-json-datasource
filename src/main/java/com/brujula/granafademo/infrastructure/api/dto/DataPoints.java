package com.brujula.granafademo.infrastructure.api.dto;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.List;

public class DataPoints {

    private String target;
    private List<List<Object>> datapoints;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public List<List<Object>> getDatapoints() {
        return datapoints;
    }

    public void setDatapoints(List<List<Object>> datapoints) {
        this.datapoints = datapoints;
    }
}
