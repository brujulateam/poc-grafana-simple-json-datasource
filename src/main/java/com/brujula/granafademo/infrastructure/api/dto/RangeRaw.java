package com.brujula.granafademo.infrastructure.api.dto;

import java.io.Serializable;

public class RangeRaw implements Serializable {

    private String from;
    private String to;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
