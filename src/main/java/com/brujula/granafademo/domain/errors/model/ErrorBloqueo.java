package com.brujula.granafademo.domain.errors.model;

import java.time.LocalDateTime;

public class ErrorBloqueo {

    private LocalDateTime time;
    private Integer errors;

    public ErrorBloqueo(final LocalDateTime time, final Integer errors) {
        setErrors(errors);
        setTime(time);
    }

    public LocalDateTime getTime() {
        return time;
    }

    public Integer getErrors() {
        return errors;
    }

    public void setErrors(final Integer errors) {
        this.errors = errors;
    }

    public void setTime(final LocalDateTime time) {
        this.time = time;
    }
}
