package com.brujula.granafademo.domain.errors.repository;

import com.brujula.granafademo.domain.errors.model.ErrorBloqueo;

import java.time.LocalDateTime;
import java.util.List;

public interface ErrorRepository {

    List<ErrorBloqueo> byIdAgency(final String agency, Integer limit, final LocalDateTime from, final LocalDateTime to);
}
