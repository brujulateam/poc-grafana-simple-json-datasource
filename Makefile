UNAME := $(shell uname)

BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

DOCKER_COMPOSE  = docker-compose -f .deployment/docker/docker-compose.doc.yml -f .deployment/docker/docker-compose.dev.yml
EXEC_APP         = $(DOCKER_COMPOSE) exec -T app

ifeq ($(UNAME), Linux)
MVN             = ./mvnw
else
MVN             = mvnw.cmd
endif

NPX             = $(EXEC_JS) npx
BUILD_DIR       = build/
DISTRIBUTE_DIR       = dist/

COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m

.DEFAULT_GOAL:=help

##@ Helpers
.PHONY: help

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[1;34m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[1;34m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Building

.PHONY: build
build: clean build-war ## Build static

build-war:
	$(MVN) -f pom.xml clean package -DskipTests

.PHONY: initialize
initialize: stop clean copy-hooks copy-environment update-dependencies build-war docker-prepare start ## Initialize this project from zero

.PHONE: copy-hooks
copy-hooks:
		rm -rf .git/hooks && cp -r .deployment/hooks .git/hooks

.PHONY: copy-environment
copy-environment:
		if [ -f .env ]; then cp .env .env.bak; fi;
		cp .env.prod .env

.PHONY: start
start: ## Start this project
	$(DOCKER_COMPOSE) up -d

docker-prepare:
	chmod go-w .deployment/docker/filebeat/config/filebeat.yml
	@$(DOCKER_COMPOSE) pull --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull

.PHONY: stop
stop: ## Stop this project
	$(DOCKER_COMPOSE) stop

.PHONY: update-dependencies
update-dependencies: ## Update pom dependencies
	$(MVN) clean -C install -DskipTests

.PHONY: clean
clean: ## Clean vendors, cache, logs, assets, war, etc.
	rm -rf target
	rm -rf report/*


.PHONY: migrations-info
migrations-info:
	docker run --rm -v $$PWD/migrations:/flyway/sql -v $$PWD/.deployment/docker/flyway/config:/flyway/conf flyway/flyway info

.PHONY: migrations-validate
migrations-validate:
	docker run --rm -v $$PWD/migrations:/flyway/sql -v $$PWD/.deployment/docker/flyway/config:/flyway/conf flyway/flyway validate

.PHONY: migrations
migrations:
	docker run --rm -v $$PWD/migrations:/flyway/sql -v $$PWD/.deployment/docker/flyway/config:/flyway/conf flyway/flyway migrate


##@ Coding standard tasks
.PHONY: lint-check
lint-check: ## Run linters
	$(DOCKER_COMPOSE) run --rm checkstyle /usr/local/lib/pmd/bin/run.sh pmd  -f textcolor -d /app/src  -R rulesets/java/quickstart.xml

.PHONY: lint-fix
lint-fix: ## Apply fix with linters
	echo "lint fix"

.PHONE: sonarqube
sonarqube: unit-test
	$(MVN) sonar:sonar -Dsonar.host.url= -Dsonar.login=

##@ Validators tasks
.PHONY: audit-check
audit-check: ## Audit dependencies
	$(MVN) org.sonatype.ossindex.maven:ossindex-maven-plugin:audit -f pom.xml

.PHONY: audit-fix
audit-fix: ## Fix audit dependencies
	$(MVN) org.sonatype.ossindex.maven:ossindex-maven-plugin:audit-aggregate -f pom.xml

.PHONY: duplicate-code
duplicate-code: ## Find duplicate code
	echo "find duplicate code"

.PHONY: optimize-dependencies
optimize-dependencies: ## Optimize dependencies
	echo "optimize-dependecies"

##@ Tests
.PHONY: tests
tests: mutant-test ## Launch unit-test, mutant-test

.PHONY: unit-test
unit-test: clean ## Launch unit test
	mvn clean test

.PHONY: mutant-test
mutant-test: clean ## Launch mutant test
	mvn clean test org.pitest:pitest-maven:mutationCoverage

##@ Deployment

.PHONY: deploy-stage
deploy-stage: ## Deploy to staging branch
	./.deployment/scripts/create-staging.sh

.PHONY: delete-stage
delete-stage:
	./.deployment/scripts/delete-staging.sh

.PHONY: deploy-develop
deploy-develop: ## Deploy to develop
	./.deployment/scripts/create-develop.sh

.PHONY: delete-develop
delete-develop:
	./.deployment/scripts/delete-develop.sh


.PHONY: publish-patch
publish-patch:
	echo "TODO"
	$(GIT) push --tags

.PHONY: publish-minor
publish-minor:
	echo "TODO"
	$(GIT) push --tags

.PHONY: publish-mayor
publish-mayor:
	echo "TODO"
	$(GIT) push --tags